# Sumi Icons

Icons provided for the [Sumi](https://gitlab.com/omnifi/sumi) web project (web applications for [Omnifi Foundation](https://omnifi.foundation)). The icons are predominently exported as scalable vector graphics in this project. 

This project is embedded into **Sumi** as a git submodule. 